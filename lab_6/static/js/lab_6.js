// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'sin') {
      print.value = Math.sin(print.value);
      erase = true; 
  } else if (x === 'cos') {
      print.value = Math.cos(print.value);
      erase = true;
  } else if (x === 'tan') {
      print.value = Math.tan(print.value);
      erase = true;           
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
$(document).ready(function() {
	var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
  ]
  var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}

  if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  }
  localStorage.setItem("themes",  JSON.stringify(themes));
  alert(localStorage.getItem("selectedTheme"));

  $("body").css("background-color", JSON.parse(localStorage.getItem("selectedTheme")).bcgColor);

  $('.my-select').select2({
    'data': JSON.parse(localStorage.getItem("themes"))
  })

   $('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var get = $(".my-select").val();
    var warnaBaru;
    // var get = document.getElementsByClassName("my-select").id;
    for(i=0; i<themes.length; i++){
      if(themes[i].id == get){
        warnaBaru = themes[i];
      }
    }
    console.log(warnaBaru);
    localStorage.setItem("selectedTheme", JSON.stringify(warnaBaru));
    console.log(localStorage.getItem("selectedTheme"));
    // localStorage.setItem("selectedTheme", JSON.stringify(selected));
    $("body").css("background-color", warnaBaru.bcgColor);
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    // [TODO] ambil object theme yang dipilih
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    // [TODO] simpan object theme tadi ke local storage selectedTheme

  })
});
// END
